package io.iovision.universe.adminapi;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.iovision.universe.entity.Category;
import io.iovision.universe.repository.CategoryRepository;

/**
 * Unit test for simple App.
 */
@SpringBootTest
public class AppTest
{
	@Autowired
	private CategoryRepository categoryRepository;

	@Test
	void contextLoads() {
		Category category = new Category();
		category.setCode("code");
		category.setUuid(UUID.randomUUID());
		categoryRepository.save(category);
	}

	@Test
	public void check(){
		List<Category> cats = categoryRepository.findAll();
		
	}
}
