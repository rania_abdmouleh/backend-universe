package io.iovision.universe.adminapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.iovision.universe.service.impl.AlbumServiceImpl;

@RestController
@RequestMapping("album")
public class AlbumController {
	
	@Autowired
	AlbumServiceImpl albumService;
	
	
	@GetMapping("/provider/{id}")
	public List<String> getAlbumsByProvider(@PathVariable("id")Long id) {
				
		List<String> galleriesResources = albumService.getAlbumsByProvider(id);	
		return galleriesResources;
	}
	
}
