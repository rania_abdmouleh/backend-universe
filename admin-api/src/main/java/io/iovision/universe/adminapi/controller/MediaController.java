package io.iovision.universe.adminapi.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import io.iovision.universe.service.impl.MediaServiceImpl;

@RestController
@RequestMapping("media")
public class MediaController {

	@Autowired
	private MediaServiceImpl mediaService;


	@GetMapping("{filename:.+}")
	@ResponseBody
	public Resource getFile(@PathVariable String filename) throws IOException {
		
		Resource file = mediaService.load(filename);
		return file;
		
	}

}
