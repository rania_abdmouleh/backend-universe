package io.iovision.universe.adminapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.iovision.universe.entity.projection.CommentProjection;
import io.iovision.universe.service.impl.CommentServiceImpl;

@RestController
@RequestMapping("comment")
public class CommentController {
	
	@Autowired
	private CommentServiceImpl commentService;

	
	@GetMapping("/provider/{id}")
	public List<CommentProjection> getCommentsByProvider(@PathVariable("id")Long id) {
		
		List<CommentProjection> listComments = this.commentService.getCommentsByProvider(id);
		return listComments;
	}
	
	@GetMapping("/user/{id}")
	public List<CommentProjection> getCommentsByUser(@PathVariable("id")Integer id) {
		
		List<CommentProjection> listComments = this.commentService.getCommentsByUser(id);
		return listComments;
	}

}
