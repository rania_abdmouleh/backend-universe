package io.iovision.universe.adminapi.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import io.iovision.universe.service.impl.ApplicationServiceImpl;

@RestController
@RequestMapping("application")
public class ApplicationController {

	@Autowired
	ApplicationServiceImpl appService;


	@PostMapping(value = "/save")
	public @ResponseBody void saveApplication(@RequestPart("application") String applicationString,
			@RequestPart("file") MultipartFile file) throws JsonMappingException, JsonProcessingException {

		appService.saveApplication(applicationString, file);

	}

}
