package io.iovision.universe.adminapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.iovision.universe.service.impl.UserServiceImpl;

@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	UserServiceImpl userService;

	@GetMapping("/activeUser")
	public void updateStatus(@RequestParam("id") Integer id, @RequestParam("active") Boolean active) {
		this.userService.activeUser(id, active);
	}
}
