package io.iovision.universe.adminapi.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.iovision.universe.entity.projection.ProviderProjection;
import io.iovision.universe.service.impl.ProviderServiceImpl;

@RestController
@RequestMapping("provider")
public class ProviderController {

	@Autowired
	ProviderServiceImpl providerService;


	@GetMapping("/activeProvider")
	public void updateStatus(@RequestParam("id") Long id, @RequestParam("active") Boolean active) {
		this.providerService.activeProvider(id, active);
	}

	@GetMapping("/detail/{id}")
	public ProviderProjection getDetaillProvider(@PathVariable("id") Long id) {
		ProviderProjection provider = this.providerService.getDetailProvider(id);
		return provider;
	}

	@GetMapping("/user/{id}")
	public List<ProviderProjection> getProvidersByUser(@PathVariable("id") Integer id) {
		List<ProviderProjection> provider = this.providerService.getProvidersByUser(id);
		return provider;
	}

}
