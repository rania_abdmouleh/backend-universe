package io.iovision.universe.adminapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.iovision.universe.entity.dto.CategoryDTO;
import io.iovision.universe.entity.projection.CategoryProjection;
import io.iovision.universe.service.impl.CategoryServiceImpl;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	CategoryServiceImpl categoryService;

	@PostMapping(value = "/save")
	public void saveCat(@RequestBody CategoryDTO categoryDTO) {

		categoryService.saveCat(categoryDTO);

	}

	@GetMapping(value = "/categories")
	public Page<CategoryProjection> getCategories(@RequestParam("id") Integer id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
		
		Page<CategoryProjection> categories = categoryService.getCategories(id, page, size);
		return categories;
		
	}

}
