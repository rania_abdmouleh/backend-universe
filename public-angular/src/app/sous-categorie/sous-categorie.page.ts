import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-sous-categorie',
  templateUrl: './sous-categorie.page.html',
  styleUrls: ['./sous-categorie.page.scss'],
})
export class SousCategoriePage implements OnInit {
  segment: string;
  constructor(public navCtrl: NavController) { }

  ngOnInit() {
  }

}
