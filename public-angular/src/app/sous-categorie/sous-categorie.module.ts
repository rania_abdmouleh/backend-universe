import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SousCategoriePageRoutingModule } from './sous-categorie-routing.module';

import { SousCategoriePage } from './sous-categorie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SousCategoriePageRoutingModule
  ],
  declarations: [SousCategoriePage]
})
export class SousCategoriePageModule {}
