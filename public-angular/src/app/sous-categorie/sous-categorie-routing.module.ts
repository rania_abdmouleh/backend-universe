import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SousCategoriePage } from './sous-categorie.page';

const routes: Routes = [
  {
    path: '',
    component: SousCategoriePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SousCategoriePageRoutingModule {}
