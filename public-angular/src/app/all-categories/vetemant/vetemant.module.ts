import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VetemantPageRoutingModule } from './vetemant-routing.module';

import { VetemantPage } from './vetemant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VetemantPageRoutingModule
  ],
  declarations: [VetemantPage]
})
export class VetemantPageModule {}
