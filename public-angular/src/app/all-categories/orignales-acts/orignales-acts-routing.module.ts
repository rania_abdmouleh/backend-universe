import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrignalesActsPage } from './orignales-acts.page';

const routes: Routes = [
  {
    path: '',
    component: OrignalesActsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrignalesActsPageRoutingModule {}
