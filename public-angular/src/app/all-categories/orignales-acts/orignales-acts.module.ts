import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrignalesActsPageRoutingModule } from './orignales-acts-routing.module';

import { OrignalesActsPage } from './orignales-acts.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrignalesActsPageRoutingModule
  ],
  declarations: [OrignalesActsPage]
})
export class OrignalesActsPageModule {}
