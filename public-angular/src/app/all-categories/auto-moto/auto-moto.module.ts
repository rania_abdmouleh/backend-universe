import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AutoMotoPageRoutingModule } from './auto-moto-routing.module';

import { AutoMotoPage } from './auto-moto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AutoMotoPageRoutingModule
  ],
  declarations: [AutoMotoPage]
})
export class AutoMotoPageModule {}
