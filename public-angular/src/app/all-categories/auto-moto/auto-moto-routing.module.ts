import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutoMotoPage } from './auto-moto.page';

const routes: Routes = [
  {
    path: '',
    component: AutoMotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AutoMotoPageRoutingModule {}
