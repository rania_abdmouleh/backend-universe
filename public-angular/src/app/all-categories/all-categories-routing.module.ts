import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllCategoriesPage } from './all-categories.page';

const routes: Routes = [
  {
    path: '',
    component: AllCategoriesPage
  },
  {
    path: 'restaurants',
    loadChildren: () => import('./restaurants/restaurants.module').then( m => m.RestaurantsPageModule)
  },
  {
    path: 'bien-etre',
    loadChildren: () => import('./bien-etre/bien-etre.module').then( m => m.BienEtrePageModule)
  },
  {
    path: 'events',
    loadChildren: () => import('./events/events.module').then( m => m.EventsPageModule)
  },
  {
    path: 'travaux',
    loadChildren: () => import('./travaux/travaux.module').then( m => m.TravauxPageModule)
  },
  {
    path: 'vetemant',
    loadChildren: () => import('./vetemant/vetemant.module').then( m => m.VetemantPageModule)
  },
  {
    path: 'sante',
    loadChildren: () => import('./sante/sante.module').then( m => m.SantePageModule)
  },
  {
    path: 'nature',
    loadChildren: () => import('./nature/nature.module').then( m => m.NaturePageModule)
  },
  {
    path: 'auto-moto',
    loadChildren: () => import('./auto-moto/auto-moto.module').then( m => m.AutoMotoPageModule)
  },
  {
    path: 'services',
    loadChildren: () => import('./services/services.module').then( m => m.ServicesPageModule)
  },
  {
    path: 'commerce',
    loadChildren: () => import('./commerce/commerce.module').then( m => m.CommercePageModule)
  },
  {
    path: 'orignales-acts',
    loadChildren: () => import('./orignales-acts/orignales-acts.module').then( m => m.OrignalesActsPageModule)
  },
  {
    path: 'culture',
    loadChildren: () => import('./culture/culture.module').then( m => m.CulturePageModule)
  },
  {
    path: 'animaux',
    loadChildren: () => import('./animaux/animaux.module').then( m => m.AnimauxPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllCategoriesPageRoutingModule {}
