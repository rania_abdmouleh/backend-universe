import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BienEtrePageRoutingModule } from './bien-etre-routing.module';

import { BienEtrePage } from './bien-etre.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BienEtrePageRoutingModule
  ],
  declarations: [BienEtrePage]
})
export class BienEtrePageModule {}
