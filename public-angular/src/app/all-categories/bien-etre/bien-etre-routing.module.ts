import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BienEtrePage } from './bien-etre.page';

const routes: Routes = [
  {
    path: '',
    component: BienEtrePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BienEtrePageRoutingModule {}
