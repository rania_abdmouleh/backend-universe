import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnimauxPageRoutingModule } from './animaux-routing.module';

import { AnimauxPage } from './animaux.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnimauxPageRoutingModule
  ],
  declarations: [AnimauxPage]
})
export class AnimauxPageModule {}
