import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnimauxPage } from './animaux.page';

const routes: Routes = [
  {
    path: '',
    component: AnimauxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnimauxPageRoutingModule {}
