package io.iovision.universe.publicapi;

import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.iovision.universe.entity.Category;
import io.iovision.universe.entity.User;
import io.iovision.universe.repository.CategoryRepository;
import io.iovision.universe.repository.UserRepository;

@SpringBootTest
class PublicApiApplicationTests {

	@Autowired
	private CategoryRepository categoryRepository;

	@Test
	void contextLoads() {
		Category category = new Category();
		category.setCode("code");
		category.setUuid(UUID.randomUUID());
		categoryRepository.save(category);
	}

	@Test
	public void check(){
		List<Category> cats = categoryRepository.findAll();
		
	}

}
