

package io.iovision.universe.publicapi.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController{
    @GetMapping("/hello")
    public String sayHell(Principal principal){
        return "hello";
    }
}