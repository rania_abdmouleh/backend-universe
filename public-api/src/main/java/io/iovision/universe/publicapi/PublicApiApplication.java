package io.iovision.universe.publicapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("io.iovision.universe")
public class PublicApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublicApiApplication.class, args);
	}

}
