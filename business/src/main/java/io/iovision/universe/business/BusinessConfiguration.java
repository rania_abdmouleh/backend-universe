package io.iovision.universe.business;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import io.iovision.universe.entity.Album;
import io.iovision.universe.repository.CategoryRepository;

@Configuration
@PropertySource("classpath:business-application.properties")
@EntityScan(basePackageClasses = Album.class)
@EnableJpaRepositories(basePackageClasses = CategoryRepository.class)
public class BusinessConfiguration
{
}
