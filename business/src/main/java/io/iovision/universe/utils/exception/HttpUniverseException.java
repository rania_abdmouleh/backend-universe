package io.iovision.universe.utils.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;

public class HttpUniverseException extends RuntimeException {
	
	private HttpStatus status;
	private String code;

	public HttpUniverseException(HttpStatus status, String code, String message){
        super(message);
        this.status = status;
        this.code = code;
    }

	public String getCode() {
		return code;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public Map<String, Object> format() {
		Map<String, Object> detail = new HashMap<>();
		detail.put("code", code);
		detail.put("message", getMessage());
		return detail;
	}
}
