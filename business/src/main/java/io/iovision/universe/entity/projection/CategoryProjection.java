package io.iovision.universe.entity.projection;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import io.iovision.universe.entity.Category;
import io.iovision.universe.entity.Media;

@Projection(name = "categories", types = { Category.class })
public interface CategoryProjection {

	Integer getId();

	ApplicationProjection getApplication();

	String getCode();

	String getLabelFr();

	UUID getUuid();

	CategoryProjection getCategory();

	Media getMedia1();

	String getImgUri();

	@Value("#{target.getCategory()?.getId()}")
	Integer getCategoryParentId();

	@Value("#{target.getApplication()?.getId()}")
	Integer getApplicationId();

}
