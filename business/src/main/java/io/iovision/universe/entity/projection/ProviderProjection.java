package io.iovision.universe.entity.projection;

import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import io.iovision.universe.entity.Provider;

@Projection(name = "providers", types = { Provider.class })
public interface ProviderProjection {

	Long getId();

	Boolean getActive();

	String getAddress();

	String getName();

	Integer getVisitNumber();

	Double getLocationLongitude();

	Double getLocationLatitude();

	String getPhone();

	Set<WorkingDayProjection> getWorkingDays();

	Set<ProvisionProjection> getProvisions();

	@Value("#{target.getMedia2()?.getName()}")
	String getLogo();

	@Value("#{target.getMedia1()?.getName()}")
	String getCouvertureImg();

	@Value("#{target.getUser()?.getFullName()}")
	String getResponsableName();

	Set<AlbumProjection> getAlbums();

}
