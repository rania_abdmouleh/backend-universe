package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the service database table.
 * 
 */
@Entity
@NamedQuery(name = "Service.findAll", query = "SELECT s FROM Service s")
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String description;
	private String name;
	private Double price;
	private Provision provision;
	private String unit;
	private Media media;

	public Service() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	// bi-directional many-to-one association to Provision
	@ManyToOne(fetch = FetchType.LAZY)
	public Provision getProvision() {
		return this.provision;
	}

	public void setProvision(Provision provision) {
		this.provision = provision;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	// bi-directional many-to-one association to Media
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "image_id")
	public Media getMedia() {
		return this.media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

}