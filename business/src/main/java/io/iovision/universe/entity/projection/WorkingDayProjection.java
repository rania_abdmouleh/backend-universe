package io.iovision.universe.entity.projection;

import java.sql.Time;

public interface WorkingDayProjection {

	Long getId();

	String getDayOfWeek();

	Time getEndPeriod1();

	Time getEndPeriod2();

	Time getStartPeriod1();

	Time getStartPeriod2();

}
