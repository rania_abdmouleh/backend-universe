package io.iovision.universe.entity.projection;

import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import io.iovision.universe.entity.Album;
import io.iovision.universe.entity.Gallery;
import io.iovision.universe.entity.Provider;

@Projection(name = "albums", types = { Album.class })
public interface AlbumProjection {

	Set<GalleryProjection> getGalleries();

}
