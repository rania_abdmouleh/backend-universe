package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;


/**
 * The persistent class for the working_days database table.
 * 
 */
@Entity
@Table(name="working_days")
@NamedQuery(name="WorkingDay.findAll", query="SELECT w FROM WorkingDay w")
public class WorkingDay implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String dayOfWeek;
	private Time endPeriod1;
	private Time endPeriod2;
	private Time startPeriod1;
	private Time startPeriod2;
	private Provider provider;

	public WorkingDay() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Column(name="day_of_week")
	public String getDayOfWeek() {
		return this.dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}


	@Column(name="end_period_1")
	public Time getEndPeriod1() {
		return this.endPeriod1;
	}

	public void setEndPeriod1(Time endPeriod1) {
		this.endPeriod1 = endPeriod1;
	}


	@Column(name="end_period_2")
	public Time getEndPeriod2() {
		return this.endPeriod2;
	}

	public void setEndPeriod2(Time endPeriod2) {
		this.endPeriod2 = endPeriod2;
	}


	@Column(name="start_period_1")
	public Time getStartPeriod1() {
		return this.startPeriod1;
	}

	public void setStartPeriod1(Time startPeriod1) {
		this.startPeriod1 = startPeriod1;
	}


	@Column(name="start_period_2")
	public Time getStartPeriod2() {
		return this.startPeriod2;
	}

	public void setStartPeriod2(Time startPeriod2) {
		this.startPeriod2 = startPeriod2;
	}


	//bi-directional many-to-one association to Provider
	@ManyToOne(fetch=FetchType.LAZY)
	public Provider getProvider() {
		return this.provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

}