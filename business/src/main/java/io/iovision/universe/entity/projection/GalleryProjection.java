package io.iovision.universe.entity.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import io.iovision.universe.entity.Gallery;

@Projection(name = "galleries", types = { Gallery.class })
public interface GalleryProjection {
	
	Long getId() ;
	
	@Value("#{target.getMedia()?.getName()}")
	String getMediaName();
	
}
