package io.iovision.universe.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;


/**
 * The persistent class for the provision database table.
 * 
 */
@Entity
@NamedQuery(name="Provision.findAll", query="SELECT p FROM Provision p")
public class Provision implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private Category category;
	private Provider provider;
	private Set<Service> services;

	public Provision() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}


	//bi-directional many-to-one association to Provider
	@ManyToOne(fetch=FetchType.LAZY)
	public Provider getProvider() {
		return this.provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	//bi-directional many-to-one association to Service
		@OneToMany(mappedBy="provision")
		public Set<Service> getServices() {
			return this.services;
		}

		public void setServices(Set<Service> services) {
			this.services = services;
		}

}