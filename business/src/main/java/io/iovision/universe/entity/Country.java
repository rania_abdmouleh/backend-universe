package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the country database table.
 * 
 */
@Entity
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private Set<City> cities;
	private Set<Provider> providers1;
	private Set<Provider> providers2;

	public Country() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	//bi-directional many-to-one association to City
	@OneToMany(mappedBy="country")
	public Set<City> getCities() {
		return this.cities;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}

	public City addCity(City city) {
		getCities().add(city);
		city.setCountry(this);

		return city;
	}

	public City removeCity(City city) {
		getCities().remove(city);
		city.setCountry(null);

		return city;
	}


	//bi-directional many-to-one association to Provider
	@OneToMany(mappedBy="country1")
	public Set<Provider> getProviders1() {
		return this.providers1;
	}

	public void setProviders1(Set<Provider> providers1) {
		this.providers1 = providers1;
	}

	public Provider addProviders1(Provider providers1) {
		getProviders1().add(providers1);
		providers1.setCountry1(this);

		return providers1;
	}

	public Provider removeProviders1(Provider providers1) {
		getProviders1().remove(providers1);
		providers1.setCountry1(null);

		return providers1;
	}


	//bi-directional many-to-one association to Provider
	@OneToMany(mappedBy="country2")
	public Set<Provider> getProviders2() {
		return this.providers2;
	}

	public void setProviders2(Set<Provider> providers2) {
		this.providers2 = providers2;
	}

	public Provider addProviders2(Provider providers2) {
		getProviders2().add(providers2);
		providers2.setCountry2(this);

		return providers2;
	}

	public Provider removeProviders2(Provider providers2) {
		getProviders2().remove(providers2);
		providers2.setCountry2(null);

		return providers2;
	}

}