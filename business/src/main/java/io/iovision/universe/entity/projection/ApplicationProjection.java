package io.iovision.universe.entity.projection;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import io.iovision.universe.entity.Application;

@Projection(name = "applications", types = { Application.class })
public interface ApplicationProjection {

	Integer getId();

	String getDescription();

	String getName();

	UUID getUuid();

	@Value("#{target.getMedia2()?.getName()}")
	String getNameLogo();
}
