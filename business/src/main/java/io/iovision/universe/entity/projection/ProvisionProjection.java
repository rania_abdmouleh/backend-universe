package io.iovision.universe.entity.projection;

import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import io.iovision.universe.entity.Provision;

@Projection(name = "provisions", types = { Provision.class })
public interface ProvisionProjection {

	String getName();

	Set<ServiceProjection> getServices();

	@Value("#{target.getCategory()?.getLabelFr()}")
	String getCategoryName();
}
