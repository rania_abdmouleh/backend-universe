package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the user_category_visit database table.
 * 
 */
@Embeddable
public class UserCategoryVisitPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private Integer userId;
	private Integer categoryId;

	public UserCategoryVisitPK() {
	}

	@Column(name="user_id", insertable=false, updatable=false)
	public Integer getUserId() {
		return this.userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name="category_id", insertable=false, updatable=false)
	public Integer getCategoryId() {
		return this.categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserCategoryVisitPK)) {
			return false;
		}
		UserCategoryVisitPK castOther = (UserCategoryVisitPK)other;
		return 
			this.userId.equals(castOther.userId)
			&& this.categoryId.equals(castOther.categoryId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userId.hashCode();
		hash = hash * prime + this.categoryId.hashCode();
		
		return hash;
	}
}