package io.iovision.universe.entity.projection;

import org.springframework.data.rest.core.config.Projection;

import io.iovision.universe.entity.Provision;
import io.iovision.universe.entity.Service;

@Projection(name = "services", types = { Service.class })
public interface ServiceProjection {

	String getName();
	Double getPrice();
	String getUnit();
}
