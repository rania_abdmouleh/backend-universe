package io.iovision.universe.entity.projection;

import java.sql.Timestamp;
import org.springframework.beans.factory.annotation.Value;


public interface CommentProjection {
	Timestamp getDate();

	String getContent();

	Integer getRating();

	@Value("#{target.getUser()?.getEmail()}")
	String getUserName();

	@Value("#{target.getProvider()?.getName()}")
	String getNameProvider();

	@Value("#{target.getProvider()?.getId()}")
	Long getIdProvider();
}
