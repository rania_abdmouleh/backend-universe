package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;


/**
 * The persistent class for the _user database table.
 * 
 */
@Entity
@Table(name="_user")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String email;
	private Double locationLatitude;
	private Double locationLongitude;
	private String phone;
	
	private String fullName;
	private Integer age;
	private String ville;
	private Boolean active;
	
	private Media media;
	private Set<Comment> comments;
	private Set<Provider> providers1;
	private Set<Media> medias;
	private Set<Notification> notifications;
	private Set<Provider> providers2;
	private Set<UserCategoryVisit> userCategoryVisits;

	public User() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	@Column(name="location_latitude")
	public Double getLocationLatitude() {
		return this.locationLatitude;
	}

	public void setLocationLatitude(Double locationLatitude) {
		this.locationLatitude = locationLatitude;
	}


	@Column(name="location_longitude")
	public Double getLocationLongitude() {
		return this.locationLongitude;
	}

	public void setLocationLongitude(Double locationLongitude) {
		this.locationLongitude = locationLongitude;
	}


	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	

	@Column(name="full_name")
	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public Integer getAge() {
		return age;
	}


	public void setAge(Integer age) {
		this.age = age;
	}


	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public Boolean getActive() {
		return active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	//bi-directional many-to-one association to Media
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="photo_id")
	public Media getMedia() {
		return this.media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}


	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="user")	
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		getComments().add(comment);
		comment.setUser(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setUser(null);

		return comment;
	}


	//bi-directional many-to-many association to Provider
	@ManyToMany(mappedBy="users")
	public Set<Provider> getProviders1() {
		return this.providers1;
	}

	public void setProviders1(Set<Provider> providers1) {
		this.providers1 = providers1;
	}


	//bi-directional many-to-one association to Media
	@OneToMany(mappedBy="user")
	public Set<Media> getMedias() {
		return this.medias;
	}

	public void setMedias(Set<Media> medias) {
		this.medias = medias;
	}

	public Media addMedia(Media media) {
		getMedias().add(media);
		media.setUser(this);

		return media;
	}

	public Media removeMedia(Media media) {
		getMedias().remove(media);
		media.setUser(null);

		return media;
	}


	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="user")
	public Set<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setUser(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setUser(null);

		return notification;
	}


	//bi-directional many-to-one association to Provider
	@OneToMany(mappedBy="user")
	public Set<Provider> getProviders2() {
		return this.providers2;
	}

	public void setProviders2(Set<Provider> providers2) {
		this.providers2 = providers2;
	}

	public Provider addProviders2(Provider providers2) {
		getProviders2().add(providers2);
		providers2.setUser(this);

		return providers2;
	}

	public Provider removeProviders2(Provider providers2) {
		getProviders2().remove(providers2);
		providers2.setUser(null);

		return providers2;
	}


	//bi-directional many-to-one association to UserCategoryVisit
	@OneToMany(mappedBy="user")
	public Set<UserCategoryVisit> getUserCategoryVisits() {
		return this.userCategoryVisits;
	}

	public void setUserCategoryVisits(Set<UserCategoryVisit> userCategoryVisits) {
		this.userCategoryVisits = userCategoryVisits;
	}

	public UserCategoryVisit addUserCategoryVisit(UserCategoryVisit userCategoryVisit) {
		getUserCategoryVisits().add(userCategoryVisit);
		userCategoryVisit.setUser(this);

		return userCategoryVisit;
	}

	public UserCategoryVisit removeUserCategoryVisit(UserCategoryVisit userCategoryVisit) {
		getUserCategoryVisits().remove(userCategoryVisit);
		userCategoryVisit.setUser(null);

		return userCategoryVisit;
	}

}