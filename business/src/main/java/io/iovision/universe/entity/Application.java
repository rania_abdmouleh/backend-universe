package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

/**
 * The persistent class for the application database table.
 * 
 */
@Entity
@NamedQuery(name = "Application.findAll", query = "SELECT a FROM Application a")
public class Application implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String description;
	private String name;
	private UUID uuid;
	private Media media1;
	private Media media2;
	private Set<Category> categories;

	public Application() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	// bi-directional many-to-one association to Media
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "icon_id")
	public Media getMedia1() {
		return this.media1;
	}

	public void setMedia1(Media media1) {
		this.media1 = media1;
	}

	// bi-directional many-to-one association to Media
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "logo_id")
	public Media getMedia2() {
		return this.media2;
	}

	public void setMedia2(Media media2) {
		this.media2 = media2;
	}

	// bi-directional many-to-one association to Category
	@OneToMany(mappedBy = "application")
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Category addCategory(Category category) {
		getCategories().add(category);
		category.setApplication(this);

		return category;
	}

	public Category removeCategory(Category category) {
		getCategories().remove(category);
		category.setApplication(null);

		return category;
	}

}