package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;


/**
 * The persistent class for the provider database table.
 * 
 */
@Entity
@NamedQuery(name="Provider.findAll", query="SELECT p FROM Provider p")
public class Provider implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Boolean active;
	private String address;
	private Timestamp creationDate;
	private Boolean draft;
	private String email;
	private String facebook;
	private String instagram;
	private String linkedin;
	private Double locationLatitude;
	private Double locationLongitude;
	private Timestamp modificationDate;
	private String name;
	private String phone;
	private Integer rating;
	private UUID uuid;
	private Integer visitNumber;
	private Set<Album> albums;
	private Set<Comment> comments;
	private Set<User> users;
	private Set<Notification> notifications;
	private User user;
	private Country country1;
	private Country country2;
	private Media media1;
	private Media media2;
	private Set<Provision> provisions;
	private Set<WorkingDay> workingDays;

	public Provider() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	@Column(name="creation_date")
	public Timestamp getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}


	public Boolean getDraft() {
		return this.draft;
	}

	public void setDraft(Boolean draft) {
		this.draft = draft;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}


	public String getInstagram() {
		return this.instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}


	public String getLinkedin() {
		return this.linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}


	@Column(name="location_latitude")
	public Double getLocationLatitude() {
		return this.locationLatitude;
	}

	public void setLocationLatitude(Double locationLatitude) {
		this.locationLatitude = locationLatitude;
	}


	@Column(name="location_longitude")
	public Double getLocationLongitude() {
		return this.locationLongitude;
	}

	public void setLocationLongitude(Double locationLongitude) {
		this.locationLongitude = locationLongitude;
	}


	@Column(name="modification_date")
	public Timestamp getModificationDate() {
		return this.modificationDate;
	}

	public void setModificationDate(Timestamp modificationDate) {
		this.modificationDate = modificationDate;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public Integer getRating() {
		return this.rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}


	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}


	@Column(name="visit_number")
	public Integer getVisitNumber() {
		return this.visitNumber;
	}

	public void setVisitNumber(Integer visitNumber) {
		this.visitNumber = visitNumber;
	}


	//bi-directional many-to-one association to Album
	@OneToMany(mappedBy="provider")
	public Set<Album> getAlbums() {
		return this.albums;
	}

	public void setAlbums(Set<Album> albums) {
		this.albums = albums;
	}

	public Album addAlbum(Album album) {
		getAlbums().add(album);
		album.setProvider(this);

		return album;
	}

	public Album removeAlbum(Album album) {
		getAlbums().remove(album);
		album.setProvider(null);

		return album;
	}


	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="provider")
	public Set<Comment> getComments() {
		return this.comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		
		getComments().add(comment);
		comment.setProvider(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setProvider(null);

		return comment;
	}


	//bi-directional many-to-many association to User
	@ManyToMany
	@JoinTable(
		name="favorite"
		, joinColumns={
			@JoinColumn(name="provider_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="user_id")
			}
		)
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}


	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="provider")
	public Set<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setProvider(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setProvider(null);

		return notification;
	}


	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	//bi-directional many-to-one association to Country
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="city_id")
	public Country getCountry1() {
		return this.country1;
	}

	public void setCountry1(Country country1) {
		this.country1 = country1;
	}


	//bi-directional many-to-one association to Country
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="country_id")
	public Country getCountry2() {
		return this.country2;
	}

	public void setCountry2(Country country2) {
		this.country2 = country2;
	}


	//bi-directional many-to-one association to Media
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cover_id")
	public Media getMedia1() {
		return this.media1;
	}

	public void setMedia1(Media media1) {
		this.media1 = media1;
	}


	//bi-directional many-to-one association to Media
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="logo_id")
	public Media getMedia2() {
		return this.media2;
	}

	public void setMedia2(Media media2) {
		this.media2 = media2;
	}


	//bi-directional many-to-one association to Provision
	@OneToMany(mappedBy="provider")
	public Set<Provision> getProvisions() {
		return this.provisions;
	}

	public void setProvisions(Set<Provision> provisions) {
		this.provisions = provisions;
	}

	public Provision addProvision(Provision provision) {
		getProvisions().add(provision);
		provision.setProvider(this);

		return provision;
	}

	public Provision removeProvision(Provision provision) {
		getProvisions().remove(provision);
		provision.setProvider(null);

		return provision;
	}


	//bi-directional many-to-one association to WorkingDay
	@OneToMany(mappedBy="provider")
	public Set<WorkingDay> getWorkingDays() {
		return this.workingDays;
	}

	public void setWorkingDays(Set<WorkingDay> workingDays) {
		this.workingDays = workingDays;
	}

	public WorkingDay addWorkingDay(WorkingDay workingDay) {
		getWorkingDays().add(workingDay);
		workingDay.setProvider(this);

		return workingDay;
	}

	public WorkingDay removeWorkingDay(WorkingDay workingDay) {
		getWorkingDays().remove(workingDay);
		workingDay.setProvider(null);

		return workingDay;
	}

}