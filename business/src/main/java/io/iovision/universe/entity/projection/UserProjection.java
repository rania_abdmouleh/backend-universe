package io.iovision.universe.entity.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import io.iovision.universe.entity.User;

@Projection(name = "users", types = { User.class })
public interface UserProjection {
	Integer getId();

	String getEmail();

	@Value("#{target.getFullName()}")
	String getFullName();

	Integer getAge();

	String getVille();

	Boolean getActive();

	@Value("#{target.getComments().size() ?: 0}")
	Integer getNbreComments();

	@Value("#{target.getProviders2().size() ?: 0}")
	Integer getNbreProviders();

	String getPhone();
}
