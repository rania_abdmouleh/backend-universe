package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_category_visit database table.
 * 
 */
@Entity
@Table(name="user_category_visit")
@NamedQuery(name="UserCategoryVisit.findAll", query="SELECT u FROM UserCategoryVisit u")
public class UserCategoryVisit implements Serializable {
	private static final long serialVersionUID = 1L;
	private UserCategoryVisitPK id;
	private Integer visitCount;
	private User user;
	private Category category;

	public UserCategoryVisit() {
	}


	@EmbeddedId
	public UserCategoryVisitPK getId() {
		return this.id;
	}

	public void setId(UserCategoryVisitPK id) {
		this.id = id;
	}


	@Column(name="visit_count")
	public Integer getVisitCount() {
		return this.visitCount;
	}

	public void setVisitCount(Integer visitCount) {
		this.visitCount = visitCount;
	}


	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}