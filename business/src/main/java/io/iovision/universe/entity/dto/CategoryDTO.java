package io.iovision.universe.entity.dto;

import java.util.UUID;

public class CategoryDTO {

	private Integer id;
	private String code;
	private String labelFr;
	private Integer applicationID;
	private Integer categoryID;
	private String imgUri;
	private UUID uuid;	

	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLabelFr() {
		return labelFr;
	}
	public void setLabelFr(String labelFr) {
		this.labelFr = labelFr;
	}
	
	public String getImgUri() {
		return imgUri;
	}
	public void setImgUri(String imgUri) {
		this.imgUri = imgUri;
	}
	public Integer getApplicationID() {
		return applicationID;
	}
	public void setApplicationID(Integer applicationID) {
		this.applicationID = applicationID;
	}
	public Integer getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(Integer categoryID) {
		this.categoryID = categoryID;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getUuid() {
		return uuid;
	}
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	
	
	
	
}
