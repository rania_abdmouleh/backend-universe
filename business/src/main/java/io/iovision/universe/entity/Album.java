package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the album database table.
 * 
 */
@Entity
@NamedQuery(name="Album.findAll", query="SELECT a FROM Album a")
public class Album implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Provider provider;
	private Set<Gallery> galleries;

	public Album() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	//bi-directional many-to-one association to Provider
	@ManyToOne(fetch=FetchType.LAZY)
	public Provider getProvider() {
		return this.provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}


	//bi-directional many-to-one association to Gallery
	@OneToMany(mappedBy="album")
	public Set<Gallery> getGalleries() {
		return this.galleries;
	}

	public void setGalleries(Set<Gallery> galleries) {
		this.galleries = galleries;
	}

	public Gallery addGallery(Gallery gallery) {
		getGalleries().add(gallery);
		gallery.setAlbum(this);

		return gallery;
	}

	public Gallery removeGallery(Gallery gallery) {
		getGalleries().remove(gallery);
		gallery.setAlbum(null);

		return gallery;
	}

}