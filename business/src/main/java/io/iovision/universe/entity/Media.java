package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the media database table.
 * 
 */
@Entity
@NamedQuery(name="Media.findAll", query="SELECT m FROM Media m")
public class Media implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String thumbnailUrl;
	private String type;
	private String url;
	private Set<User> users;
	private Set<Application> applications1;
	private Set<Application> applications2;
	private Set<Category> categories1;
	private Set<Category> categories2;
	private Set<Gallery> galleries;
	private User user;
	private Set<Provider> providers1;
	private Set<Provider> providers2;
	private Set<Service> services;

	public Media() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Column(name="thumbnail_url")
	public String getThumbnailUrl() {
		return this.thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}


	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="media")
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setMedia(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setMedia(null);

		return user;
	}


	//bi-directional many-to-one association to Application
	@OneToMany(mappedBy="media1")
	public Set<Application> getApplications1() {
		return this.applications1;
	}

	public void setApplications1(Set<Application> applications1) {
		this.applications1 = applications1;
	}

	public Application addApplications1(Application applications1) {
		getApplications1().add(applications1);
		applications1.setMedia1(this);

		return applications1;
	}

	public Application removeApplications1(Application applications1) {
		getApplications1().remove(applications1);
		applications1.setMedia1(null);

		return applications1;
	}


	//bi-directional many-to-one association to Application
	@OneToMany(mappedBy="media2")
	public Set<Application> getApplications2() {
		return this.applications2;
	}

	public void setApplications2(Set<Application> applications2) {
		this.applications2 = applications2;
	}

	public Application addApplications2(Application applications2) {
		getApplications2().add(applications2);
		applications2.setMedia2(this);

		return applications2;
	}

	public Application removeApplications2(Application applications2) {
		getApplications2().remove(applications2);
		applications2.setMedia2(null);

		return applications2;
	}


	//bi-directional many-to-one association to Category
	@OneToMany(mappedBy="media1")
	public Set<Category> getCategories1() {
		return this.categories1;
	}

	public void setCategories1(Set<Category> categories1) {
		this.categories1 = categories1;
	}

	public Category addCategories1(Category categories1) {
		getCategories1().add(categories1);
		categories1.setMedia1(this);

		return categories1;
	}

	public Category removeCategories1(Category categories1) {
		getCategories1().remove(categories1);
		categories1.setMedia1(null);

		return categories1;
	}


	//bi-directional many-to-one association to Category
	@OneToMany(mappedBy="media2")
	public Set<Category> getCategories2() {
		return this.categories2;
	}

	public void setCategories2(Set<Category> categories2) {
		this.categories2 = categories2;
	}

	public Category addCategories2(Category categories2) {
		getCategories2().add(categories2);
		categories2.setMedia2(this);

		return categories2;
	}

	public Category removeCategories2(Category categories2) {
		getCategories2().remove(categories2);
		categories2.setMedia2(null);

		return categories2;
	}


	//bi-directional many-to-one association to Gallery
	@OneToMany(mappedBy="media")
	public Set<Gallery> getGalleries() {
		return this.galleries;
	}

	public void setGalleries(Set<Gallery> galleries) {
		this.galleries = galleries;
	}

	public Gallery addGallery(Gallery gallery) {
		getGalleries().add(gallery);
		gallery.setMedia(this);

		return gallery;
	}

	public Gallery removeGallery(Gallery gallery) {
		getGalleries().remove(gallery);
		gallery.setMedia(null);

		return gallery;
	}


	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	//bi-directional many-to-one association to Provider
	@OneToMany(mappedBy="media1")
	public Set<Provider> getProviders1() {
		return this.providers1;
	}

	public void setProviders1(Set<Provider> providers1) {
		this.providers1 = providers1;
	}

	public Provider addProviders1(Provider providers1) {
		getProviders1().add(providers1);
		providers1.setMedia1(this);

		return providers1;
	}

	public Provider removeProviders1(Provider providers1) {
		getProviders1().remove(providers1);
		providers1.setMedia1(null);

		return providers1;
	}


	//bi-directional many-to-one association to Provider
	@OneToMany(mappedBy="media2")
	public Set<Provider> getProviders2() {
		return this.providers2;
	}

	public void setProviders2(Set<Provider> providers2) {
		this.providers2 = providers2;
	}

	public Provider addProviders2(Provider providers2) {
		getProviders2().add(providers2);
		providers2.setMedia2(this);

		return providers2;
	}

	public Provider removeProviders2(Provider providers2) {
		getProviders2().remove(providers2);
		providers2.setMedia2(null);

		return providers2;
	}


	//bi-directional many-to-one association to Service
	@OneToMany(mappedBy="media")
	public Set<Service> getServices() {
		return this.services;
	}

	public void setServices(Set<Service> services) {
		this.services = services;
	}

	public Service addService(Service service) {
		getServices().add(service);
		service.setMedia(this);

		return service;
	}

	public Service removeService(Service service) {
		getServices().remove(service);
		service.setMedia(null);

		return service;
	}

}