package io.iovision.universe.entity;

import java.io.Serializable;
import javax.persistence.*;


import java.util.Set;
import java.util.UUID;


/**
 * The persistent class for the category database table.
 * 
 */
@Entity
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String code;
	private String labelFr;
	private UUID uuid;	
	private Application application;
	private Category category;
	private Set<Category> categories;
	private Media media1;
	private Media media2;
	private Set<Provision> provisions;
	private Set<UserCategoryVisit> userCategoryVisits;
	private String imgUri;
	
	public Category() {
	}
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	@Column(name="label_fr")
	public String getLabelFr() {
		return this.labelFr;
	}

	public void setLabelFr(String labelFr) {
		this.labelFr = labelFr;
	}


	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	
	

	@Column(name="img_uri")
	public String getImgUri() {
		return imgUri;
	}


	public void setImgUri(String imgUri) {
		this.imgUri = imgUri;
	}


	//bi-directional many-to-one association to Application
	@ManyToOne(fetch=FetchType.LAZY)
	public Application getApplication() {
		return this.application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}


	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parent_category_id")
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}


	//bi-directional many-to-one association to Category
	@OneToMany(mappedBy="category")
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Category addCategory(Category category) {
		getCategories().add(category);
		category.setCategory(this);

		return category;
	}

	public Category removeCategory(Category category) {
		getCategories().remove(category);
		category.setCategory(null);

		return category;
	}


	//bi-directional many-to-one association to Media
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="icon_id")
	public Media getMedia1() {
		return this.media1;
	}

	public void setMedia1(Media media1) {
		this.media1 = media1;
	}


	//bi-directional many-to-one association to Media
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="image_id")
	public Media getMedia2() {
		return this.media2;
	}

	public void setMedia2(Media media2) {
		this.media2 = media2;
	}


	//bi-directional many-to-one association to Provision
	@OneToMany(mappedBy="category")
	public Set<Provision> getProvisions() {
		return this.provisions;
	}

	public void setProvisions(Set<Provision> provisions) {
		this.provisions = provisions;
	}

	public Provision addProvision(Provision provision) {
		getProvisions().add(provision);
		provision.setCategory(this);

		return provision;
	}

	public Provision removeProvision(Provision provision) {
		getProvisions().remove(provision);
		provision.setCategory(null);

		return provision;
	}


	//bi-directional many-to-one association to UserCategoryVisit
	@OneToMany(mappedBy="category")
	public Set<UserCategoryVisit> getUserCategoryVisits() {
		return this.userCategoryVisits;
	}

	public void setUserCategoryVisits(Set<UserCategoryVisit> userCategoryVisits) {
		this.userCategoryVisits = userCategoryVisits;
	}

	public UserCategoryVisit addUserCategoryVisit(UserCategoryVisit userCategoryVisit) {
		getUserCategoryVisits().add(userCategoryVisit);
		userCategoryVisit.setCategory(this);

		return userCategoryVisit;
	}

	public UserCategoryVisit removeUserCategoryVisit(UserCategoryVisit userCategoryVisit) {
		getUserCategoryVisits().remove(userCategoryVisit);
		userCategoryVisit.setCategory(null);

		return userCategoryVisit;
	}

}