package io.iovision.universe.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.iovision.universe.entity.Provider;
import io.iovision.universe.entity.projection.ProviderProjection;
import io.iovision.universe.repository.ProviderRepository;

@Service
public class ProviderServiceImpl {

	@Autowired
	ProviderRepository repository;
	
   
	@Transactional
	public void activeProvider(Long id, Boolean active) {
		repository.updateActive(id, active);
		
	}


	public ProviderProjection getDetailProvider(Long id) {
		ProviderProjection p = repository.getDetailProvider(id);
		/*p.getComments().stream().forEach(comment -> {
			comment.setProvider(null);
			comment.setUser(null);
		});*/
		return p;
	}


	public List<ProviderProjection> getProvidersByUser(Integer id) {
		List<ProviderProjection> p = repository.getProvidersByUser(id);
		return p;
	}
    
	
}
