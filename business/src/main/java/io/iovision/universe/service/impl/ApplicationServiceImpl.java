package io.iovision.universe.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.iovision.universe.entity.Application;
import io.iovision.universe.entity.Media;
import io.iovision.universe.repository.ApplicationRepository;

@Service
public class ApplicationServiceImpl {

	@Autowired
	ApplicationRepository repo;

	@Autowired
	MediaServiceImpl mediaService;

	public void saveApplication(String applicationString, MultipartFile file)
			throws JsonMappingException, JsonProcessingException {

		Application appToSave = new Application();
		Media mediaSaved = new Media();

		ObjectMapper mapper = new ObjectMapper();
		Application application = mapper.readValue(applicationString, Application.class);

		Integer idApp = application.getId();
		boolean isExiste = false;

		if (idApp != null)
			isExiste = this.repo.existsById(idApp);

		if (file.getSize() > 0)
			mediaSaved = mediaService.saveMedia(file);

		if (!isExiste) {

			UUID sameUuid = application.getUuid();
			appToSave.setUuid(sameUuid);
			appToSave.setName(application.getName());
			appToSave.setDescription(application.getDescription());

			if (file.getSize() > 0) {
				appToSave.setMedia2(mediaSaved);
			}

		} else {

			Optional<Application> appToUpdate = repo.findById(application.getId());

			UUID uuid = appToUpdate.get().getUuid();
			appToUpdate.get().setUuid(uuid);

			appToUpdate.get().setName(application.getName());
			appToUpdate.get().setDescription(application.getDescription());

			if (file.getSize() > 0) {
				appToUpdate.get().setMedia2(mediaSaved);
			}
			appToSave = appToUpdate.get();
		}

		repo.save(appToSave);

	}

}
