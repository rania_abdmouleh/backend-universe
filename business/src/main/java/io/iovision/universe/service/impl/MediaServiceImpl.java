package io.iovision.universe.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import io.iovision.universe.entity.Media;
import io.iovision.universe.repository.MediaRepository;
import liquibase.util.file.FilenameUtils;

@Service
public class MediaServiceImpl {

	@Value("${upload.path}")
	private String uploadPath;

	@Autowired
	MediaRepository repo;

	String racinePath = System.getProperty("java.io.tmpdir");

	@PostConstruct
	public void init() {
		try {

			Files.createDirectories(Paths.get(racinePath + uploadPath));
		} catch (IOException e) {
			throw new RuntimeException("Could not create upload folder!");
		}
	}

	public Media save(MultipartFile file) {
		try {

			Path root = Paths.get(racinePath + uploadPath);

			UUID uuid = UUID.randomUUID();
			String uuidString = uuid.toString();

			String typeMedia = FilenameUtils.getExtension(file.getOriginalFilename());

			String nameFile = uuidString + '.' + typeMedia;

			Media media = new Media();
			media.setUrl(nameFile);
			media.setType(typeMedia);
			media.setName(nameFile);

			if (!Files.exists(root)) {
				init();

			}
			Files.copy(file.getInputStream(), root.resolve(nameFile));

			repo.save(media);

			return media;
		} catch (Exception e) {
			throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		}

	}

	public Resource load(String filename) throws IOException {
		try {
			Path file = Paths.get(racinePath + uploadPath).resolve(filename);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("Could not read the file!");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}

	public Media saveMedia(MultipartFile file) {
		Media mediaToSave = this.save(file);
		return mediaToSave;

	}

}
