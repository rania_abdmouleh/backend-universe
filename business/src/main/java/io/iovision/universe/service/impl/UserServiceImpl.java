package io.iovision.universe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.iovision.universe.repository.UserRepository;

@Service
public class UserServiceImpl {

	@Autowired
	UserRepository repository;

	@Transactional
	public void activeUser(Integer id, Boolean active) {
		repository.updateActive(id, active);
	}

}
