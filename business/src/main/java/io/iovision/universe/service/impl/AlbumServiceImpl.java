package io.iovision.universe.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import io.iovision.universe.entity.projection.AlbumProjection;
import io.iovision.universe.repository.AlbumRepository;

@Service
public class AlbumServiceImpl {

	@Autowired
	AlbumRepository albumRepo;

	
	public List<String> getAlbumsByProvider(Long id) {

		List<String> galleriesResources = new ArrayList<String>();
		List<AlbumProjection> listGalleries = this.albumRepo.getGalleryByProvider(id);
		for (int i = 0; i < listGalleries.size(); i++) {
			listGalleries.get(i).getGalleries().stream().forEach(gallery -> {

				galleriesResources.add(gallery.getMediaName());
			});
		}

		return galleriesResources;
	}

}
