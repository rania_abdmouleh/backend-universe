package io.iovision.universe.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.iovision.universe.entity.projection.CommentProjection;
import io.iovision.universe.repository.CommentRepository;

@Service
public class CommentServiceImpl {

	@Autowired
	CommentRepository repo;

	public List<CommentProjection> getCommentsByProvider(Long id) {
		List<CommentProjection> listComments = repo.getCommentsByProvider(id);
		return listComments;
	}

	public List<CommentProjection> getCommentsByUser(Integer id) {
		List<CommentProjection> listComments = repo.getCommentsByUser(id);
		return listComments;
	}

}
