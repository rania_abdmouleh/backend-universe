package io.iovision.universe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import io.iovision.universe.entity.Application;
import io.iovision.universe.entity.Category;
import io.iovision.universe.entity.dto.CategoryDTO;
import io.iovision.universe.entity.projection.CategoryProjection;
import io.iovision.universe.repository.ApplicationRepository;
import io.iovision.universe.repository.CategoryRepository;

@Service
public class CategoryServiceImpl {

	@Autowired
	CategoryRepository repo;

	@Autowired
	ApplicationRepository applicationRepo;

	public void saveCat(CategoryDTO categoryDTO) {

		Category categoryToSave = new Category();
		Application appToUpdate = new Application();
		Category category = new Category();

		if (categoryDTO.getId() != null) {
			categoryToSave = repo.findById(categoryDTO.getId()).get();
		} else {
			categoryToSave.setUuid(categoryDTO.getUuid());
		}

		categoryToSave.setCode(categoryDTO.getCode());
		categoryToSave.setImgUri(categoryDTO.getImgUri());
		categoryToSave.setLabelFr(categoryDTO.getLabelFr());

		if (categoryDTO.getApplicationID() != null) {
			appToUpdate = applicationRepo.findById(categoryDTO.getApplicationID()).get();
			categoryToSave.setApplication(appToUpdate);
		} else {
			categoryToSave.setApplication(null);
		}

		if (categoryDTO.getCategoryID() != null) {
			category = repo.findById(categoryDTO.getCategoryID()).get();
			categoryToSave.setCategory(category);
		} else {
			categoryToSave.setCategory(null);
		}

		repo.save(categoryToSave);

	}

	public Page<CategoryProjection> getCategories(@RequestParam("id") Integer id,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
		Pageable paging = PageRequest.of(page, size);
		Page<CategoryProjection> categories = repo.getCategoriesByCategory(id, paging);
		return categories;
	}

}
