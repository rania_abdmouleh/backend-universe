package io.iovision.universe.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import io.iovision.universe.entity.Comment;
import io.iovision.universe.entity.projection.CommentProjection;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

	@Query("select c from Comment c where c.provider.id=:id")
	List<CommentProjection> getCommentsByProvider(@Param("id") Long id);

	@Query("select c from Comment c where c.user.id=:id")
	List<CommentProjection> getCommentsByUser(@Param("id") Integer id);
}
