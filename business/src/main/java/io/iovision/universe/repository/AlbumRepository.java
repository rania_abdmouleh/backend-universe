package io.iovision.universe.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.iovision.universe.entity.Album;
import io.iovision.universe.entity.Media;
import io.iovision.universe.entity.projection.AlbumProjection;
import io.iovision.universe.entity.projection.CommentProjection;
import io.iovision.universe.entity.projection.GalleryProjection;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer> {

	@Query("select a from Album a where a.provider.id=:id")
	List<AlbumProjection> getGalleryByProvider(@Param("id")Long id);
}
