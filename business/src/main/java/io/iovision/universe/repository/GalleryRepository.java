package io.iovision.universe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.iovision.universe.entity.Gallery;

@Repository
public interface GalleryRepository extends  JpaRepository<Gallery, Integer> {

}
