package io.iovision.universe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.iovision.universe.entity.Media;

@Repository
public interface MediaRepository extends JpaRepository<Media, Integer> {

}
