package io.iovision.universe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.iovision.universe.entity.WorkingDay;

@Repository
public interface WorkingDayRepository extends JpaRepository<WorkingDay, Long>{

	
}
