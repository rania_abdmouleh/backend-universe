package io.iovision.universe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import io.iovision.universe.entity.Provider;
import io.iovision.universe.entity.projection.ProviderProjection;
import io.iovision.universe.entity.projection.ProvisionProjection;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Integer> {

	 	@Modifying
	    @Query("update Provider p set active=?2 where p.id=?1")
	    void updateActive(Long id,Boolean active);
	 	
	 	@Query("select p from Provider p where p.id=:id")
	 	ProviderProjection getDetailProvider(@Param("id") Long id);
	 	
	 	
	 	@Query("select p from Provider p where p.user.id=:id")
	 	List<ProviderProjection> getProvidersByUser(@Param("id") Integer id);
	 	
	 	
		@Query("select p from Provider p")
	 	List<ProviderProjection> getAllProviders();
		
		
}
