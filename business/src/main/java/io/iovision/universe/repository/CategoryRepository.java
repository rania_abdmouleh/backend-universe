package io.iovision.universe.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import io.iovision.universe.entity.Category;
import io.iovision.universe.entity.projection.CategoryProjection;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

	@Transactional
	@Query(value = "select c from Category  c where c.category.id = :idCat")
	Page<CategoryProjection> getCategoriesByCategory(Integer idCat, Pageable pageable);

}
