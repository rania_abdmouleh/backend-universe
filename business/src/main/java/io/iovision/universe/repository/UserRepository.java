package io.iovision.universe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import io.iovision.universe.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	@Modifying
	@Query("update User user set active=?2 where user.id=?1")
	void updateActive(Integer id, Boolean active);

}
